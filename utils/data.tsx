const description =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
export const data = [
  {
    title: "Australia",
    description: `1 ${description}`,
    image: "https://source.unsplash.com/random/1920x1080",
    buttonColor: "#3F1D21",
    items: [
      {
        title: "1",
        rate: 4,
        image: "https://source.unsplash.com/random/470x698?1"
      },
      {
        title: "2",
        rate: 4,
        image: "https://source.unsplash.com/random/470x698?2"
      },
      {
        title: "3",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?3"
      },
      {
        title: "4",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?4"
      }
    ]
  },
  {
    title: "Europe",
    description: `2 ${description}`,
    image: "https://source.unsplash.com/random/1920x1080?space",
    buttonColor: "#2F704E",
    items: [
      {
        title: "11",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?6"
      },
      {
        title: "22",
        rate: 4,
        image: "https://source.unsplash.com/random/4480×5600?7"
      },
      {
        title: "33",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?8"
      },
      {
        title: "44",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?9"
      }
    ]
  },
  {
    title: "Africa",
    description: `3 ${description}`,
    image: "https://source.unsplash.com/random/1920x1080?africa",
    buttonColor: "#3F1D21",
    items: [
      {
        title: "11",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?10"
      },
      {
        title: "22",
        rate: 4,
        image: "https://source.unsplash.com/random/4480×5600?11"
      },
      {
        title: "33",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?12"
      },
      {
        title: "44",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?13"
      }
    ]
  },
  {
    title: "Asia",
    description: `4 ${description}`,
    image: "https://source.unsplash.com/random/1920x1080?asia",
    buttonColor: "#2F704E",
    items: [
      {
        title: "11",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?space"
      },
      {
        title: "22",
        rate: 4,
        image: "https://source.unsplash.com/random/4480×5600?14"
      },
      {
        title: "33",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?15"
      },
      {
        title: "44",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?16"
      }
    ]
  },
  {
    title: "America",
    description: `5 ${description}`,
    image: "https://source.unsplash.com/random/1920x1080?america",
    buttonColor: "#3F1D21",
    items: [
      {
        title: "11",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?17"
      },
      {
        title: "22",
        rate: 4,
        image: "https://source.unsplash.com/random/4480×5600?18"
      },
      {
        title: "33",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?19"
      },
      {
        title: "44",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?20"
      }
    ]
  },
  {
    title: "Arctic",
    description: `6 ${description}`,
    image: "https://source.unsplash.com/random/1920x1080?arctic",
    buttonColor: "#2F704E",

    items: [
      {
        title: "11",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?21"
      },
      {
        title: "22",
        rate: 4,
        image: "https://source.unsplash.com/random/4480×5600?22"
      },
      {
        title: "33",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?23"
      },
      {
        title: "44",
        rate: 4,
        image: "https://source.unsplash.com/random/468x600?24"
      }
    ]
  }
];
